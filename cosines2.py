"""
Timings:


sage: %timeit cosines.test(-1+I,1,4000)
100 loops, best of 3: 5.48 ms per loop

"""

#import cython
from sage.rings.complex_number import ComplexNumber
from sage.rings.complex_field import ComplexField
from sage.all import cos
#from sage.plot.complex_plot import complex_to_rgb
#import numpy as np
#cimport numpy as np # for the special numpy stuff

CC = ComplexField(256)
# @cython.profile(False)
# cdef ComplexNumber f_internal(ComplexNumber b,
#                   ComplexNumber x0,
#                   int iterations=50):
#     cdef double z_real = 0., z_imag = 0.
#     cdef int i
#     x = ComplexNumber(CC,x0.real(),x0.imag())
#     b = ComplexNumber(CC,b.real(),b.imag())

#     for i in range(0, iterations):
#         x = x.cos()**b
#     return complex(x.real(),x.imag())


# def f(b,x0,N):
#     return f_internal(b,x0,N)

def f2(b, x = CC(1), iterations = 100, h = cos):
    """

    sage: %timeit cosines2.f2(-1+I,1,4000)
    1 loops, best of 3: 537 ms per loop
    """
    b = CC(b)
    x = CC(x)
    for i in range(iterations):
        x = CC(h(x)**b)
    # for j in range(show_next+1):
    #     # print values of x with only 32 bits of precision
    #     print "{0}th iterate (approx.): {1}".format(iterations+j,x.n(32))
    #     x = CC(h(x)**b)
    return x



def estimate_period_pyx(b,seed,iterations,h,max_period=12,epz=float(10**(-8))):
    x0 = f2(b, seed, iterations, h=h)
    #make a list
    D = []
    x = x0
    for j in range(max_period):
        x = CC(h(x)**b)
        if float((x0-x).abs()) < epz:
            return x0,j+1

    # no period found
    return x0,None

    
