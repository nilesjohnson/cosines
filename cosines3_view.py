"""
plot iterates of cos^b

Niles Johnson


EXAMPLE:

sage: load('cosines3_view.py')
sage: P = PeriodPlot(**test_P)
sage: P.save(chunks=4)



"""

import sys
from PIL import Image
gen_kwds = {'iterations':1300,
        'plot_points':2**10,
        'chunk_size':2**7,
        'eval_range':(-8,8),
        'dome':False,
        'seed_rad':0
        }

#from cosines2 import*
#load('cosines2.pyx')

#CC = sage.rings.complex_mpc.MPComplexField()
#CC = ComplexField(64)  #breaks horribly

import platform
hostname = platform.node()
hostname0 = hostname.split('.')[0]

import numpy as np




##
## cosines3.pyx
##

#from sage.rings.complex_number import ComplexNumber
#from sage.rings.complex_field import ComplexField
#from sage.all import cos
#from sage.plot.complex_plot import complex_to_rgb
#import numpy as np
#cimport numpy as np # for the special numpy stuff

#CF = ComplexField(256) # slower
CC = CDF # faster


# @cython.profile(False)
# cdef ComplexNumber f_internal(ComplexNumber b,
#                   ComplexNumber x0,
#                   int iterations=50):
#     cdef double z_real = 0., z_imag = 0.
#     cdef int i
#     x = ComplexNumber(CC,x0.real(),x0.imag())
#     b = ComplexNumber(CC,b.real(),b.imag())

#     for i in range(0, iterations):
#         x = x.cos()**b
#     return complex(x.real(),x.imag())


# def f(b,x0,N):
#     return f_internal(b,x0,N)

def f_iter(p, a = 1, field = CDF, iterations = 100, f = cos, return_next=1):
    """
    iterate the function h^p

    sage: %timeit f_iter(1+I, 1, CF, 500, cos, 1)
    10 loops, best of 3: 24.3 ms per loop

    sage: %timeit f_iter(1+I, 1, CDF, 500, cos, 1)
    1000 loops, best of 3: 866 µs per loop
    """
    p = field(p)
    a = field(a)
    for i in range(iterations):
        a = field(f(a)**p)
    D = []
    for i in range(return_next):
        a = field(f(a)**p)
        D.append(a)
    return D

#def get_period_0(b,seed,iterations,h,max_period=20,epz=float(10**(-8))):
def get_period_0(D, max_period=12, prec=32):
    """
    initial version; based on estimate_period_pyx

    it seems to be not as good
    """
    #x0 = f2(b, seed, iterations, h=h)
    x0 = D[0]
    epz = 10**(-1*prec)
    #make a list
    #D = []
    #x = x0
    #for j in range(max_period):
    for j in range(1,len(D)):
        x = D[j]
        if float((x0-x).abs()) < epz:
            return j

    # no period found
    return 0


def get_period(D, max_period=12, prec=32):
    """
    Given list D, find minimal n such that D has period n

    prec = bits of precision

    returns 0 if no period found
    """
    if len(D) < 2*max_period:
        raise ValueError("input list needs to be 2 times longer than max_period")
    E = [d.n(prec=prec) for d in D]
    #print(E)
    for s in range(1,max_period+1):
        #print("testing s = {0}".format(s))
        # s = block size
        for j in range(s):
            #print(" j = {0}".format(j))
            #print(E[j] - E[s+j])
            if (E[j] - E[s+j]).norm() != 0:
                break
            #if (E[j] - E[2*s+j]).norm() != 0:
            #    break
            return s
    return 0


def iter_per(max_period, prec, repeat_iter, **kwds):
    """
    iterate and get period
    """
    for r in range(repeat_iter):
        D = f_iter(**kwds)
        s = get_period(D,max_period,prec)
        if s != 0:
            return s
        kwds['a'] = D[0]
    return 0

def cos_tay2(x):
    return 1 - x**2/2
def cos_tay4(x):
    return 1 - x**2/2 + x**4/24
def cos3(x):
    return cos(3*x)
def cos17(x):
    return cos(17*x)
def cos1p2(x):
    return cos(1.2*x)


pf_args = {'max_period': 15,
                'prec': 30,
                'repeat_iter': 1,
                'a': 1,
                'field': CDF,
                'iterations': 6000,
                'f': cos,
                'return_next': 42,
}
test_P = {'period_func': iter_per,
          'period_func_args': pf_args,
          'plot_points': 200,
          'eval_range': (-8,8),
}
good_P = {'period_func': iter_per,
          'period_func_args': pf_args,
          'plot_points': 600,
          'eval_range': (-8,8),
}
large_P = {'period_func': iter_per,
           'period_func_args': pf_args,
           'plot_points': 6000,
           'eval_range': (-50,50),
}
close_P = {'period_func': iter_per,
           'period_func_args': pf_args,
           'plot_points': 6000,
           'eval_range': (-4,4),
}
online_P = {'period_func': iter_per,
          'period_func_args': pf_args,
          'plot_points': 1600,
          'eval_range': (-4,4),
}
online_P['period_func_args']['iterations'] = 8000
          
if hostname0 == 'n16178':
    NUMPROC = 8
elif hostname0 == 'mth-kepler':
    NUMPROC = 35
else:
    NUMPROC = 4 #a guess

print("Machine name: {0}\nNUMPROC: {1}".format(hostname0,NUMPROC))










 






from datetime import timedelta
def estimate_time(nprocs,proctime,ncpus=None):
    """
    Estimate time for parallel computation with ncpus, nprocs, and each process taking
    proctime.

    proctime is in seconds.

    Estimate is a rough guide -- more accurate when nprocs is very large compared to ncpus
    """
    if ncpus is None:
        ncpus = sage.parallel.ncpus.ncpus()
    s = proctime*nprocs/RDF(min(nprocs,ncpus))
    return str(timedelta(seconds=float(s)))


from sage.plot.misc import setup_for_eval_on_grid
from sage.plot.all import Graphics
from sage.plot.complex_plot import ComplexPlot, complex_to_rgb
#from scipy.misc import toimage
import time

import json

def make_online():
    lp = limit_points_json()
    P = PeriodPlot(**online_P)
    P.save(chunks=32)

def limit_points_json(outfile=None):
    # to make the accompanying picture, adjust the parameters of online_P to match
    maxp = 15
    prec = 30
    iterat = 8000
    plot_points = 800
    lpj_pf_args = {
        'a': 1,
        'field': CDF,
        'iterations': iterat,
        'f': cos,
        'return_next': 2*maxp,
    }
    eval_range = (-4,4)
    xysetup = setup_for_eval_on_grid([],[eval_range],plot_points=plot_points)
    xvals = srange(*xysetup[1][0], include_endpoint=True)
    yvals = xvals[plot_points/2 - 2:]
    limit_points = {}
    for i in range(len(xvals)):
        x = xvals[i]
        for j in range(len(yvals)):
            y = yvals[j]     
            D = f_iter(p = x+I*y, **lpj_pf_args)
            s = get_period(D,max_period=15,prec=30)
            if s > 0:
                L = D[0:s]
            else:
                L = D
            #L.sort()
            l_data = {'N':s,
                      'L':[str(_.n(16)) for _ in L],
                      'M':[(float(_.real()),float(_.imag())) for _ in L],
                      'xy':(x,y)
            }
            limit_points["{0},{1}".format(i,j)] = l_data

    if outfile is None:
        outfile = "limit_points_{0}_{1}_{2}.json".format(eval_range[1],plot_points,iterat)
    jsonfile = open(outfile,'w')
    jsonfile.write("var limit_data = ")
    jsonfile.write(json.dumps(limit_points))
    jsonfile.write(";")
    jsonfile.close()
    return limit_points

    
    
class PeriodPlot(SageObject):
    def __init__(self, period_func, period_func_args,
                 plot_points=200, eval_range=(-4,4), dome=False,
                 seed_rad=.05, upper_half=True, y_range=None):
        """
        period_func is a function which computes periods
        period_func_args is a dict of keyword arguments for it        
        """
        self.pf = period_func
        self.pf_args = period_func_args
        self.max_period = self.pf_args['max_period']
        self.field = self.pf_args['field']
        
        self.plot_points = plot_points
        self.dome = dome # only works for eval_range centered at 0
        self.dome_rad2 = float(eval_range[1])**2

        self.seed = self.pf_args['a']
        self.seed_rad2 = seed_rad**2

        #self.f = cosines.test  #broken

        self.ranges = (eval_range,eval_range)
        xysetup = setup_for_eval_on_grid([],[eval_range],plot_points=plot_points)
        self.xvals = srange(*xysetup[1][0], include_endpoint=True)
        if y_range is not None:
            ysetup = setup_for_eval_on_grid([],[y_range],plot_points=plot_points)
            self.yvals  = srange(*ysetup[1][0], include_endpoint=True)[::-1] # somewhere else the y range is flipped
        elif upper_half:
            self.yvals = self.xvals[0:int(self.plot_points/2) + 2]
        else:
            self.yvals = self.xvals #square image

        #self.out_numbers = np.zeros((self.plot_points,self.plot_points))
        self.color_rows = [[tuple([0,0,0]) for _ in self.xvals] for _ in self.yvals]

        
        self.palette = [(0,0,0)]
        for k in range(1,self.max_period+1):
            h = float(3.1*(k-1)/(self.max_period))
            s = .6 + .35*(1.0*k/self.max_period)
            v = .95 - .5*(1.0*k/self.max_period)
            Crgb = Color(h-.02,s,v, space='hsv').rgb()
            self.palette.append(tuple([int(255*x) for x in Crgb]))
        self.palette += [(255,255,255)]

    def get_period(self,p):
        """
        compute period for point p
        """
        if self.seed_rad2 > 0 and (self.seed - p).norm() < self.seed_rad2:
            return self.max_period+1

        return self.pf(p=p,**self.pf_args)
        
    def write_progress(self,k,N,width=40):
        """
        write progress: k steps of N total
        """
        pdashes = int(width*k/N)
        bar = "-"*pdashes+" "*(width-pdashes)
        msg = "{0:03d}/{1:03d} [{2}]".format(k,N,bar)
        sys.stdout.write(msg)
        sys.stdout.flush()
        sys.stdout.write("\b"*len(msg))
        sys.stdout.flush()

    @parallel(NUMPROC)
    def eval_on_chunk(self,chunk_start,chunk_size):
        """
        piece
        """
        periods = [[self.get_period(self.field(a,-1*b)) for a in self.xvals] \
                   for b in self.yvals[chunk_start:chunk_start+chunk_size]]
        return periods

    def color_chunk(self,periods,chunk_start,chunk_size):
        for j in range(chunk_start,chunk_start+chunk_size):
            try:
                self.color_rows[j] = [self.palette[k] for k in periods[j-chunk_start]]
            except IndexError:
                pass
            
    def save(self,name=None,chunks=1,save_partial=True):
        if name is None:
            name = tmp_filename(ext='.png')
        print("saving to {0}..".format(name))

        chunk_size = int(self.plot_points/chunks)
        im = Image.new('RGB',(self.plot_points,self.plot_points))
        if chunk_size*chunks < self.plot_points:
            chunks += 1
        self.write_progress(0,chunks)

        chunk_data = [(c*chunk_size,chunk_size) for c in range(chunks)]
        numdone = 0
        for arg,out in self.eval_on_chunk(chunk_data):
            chunk_start,chunk_size = arg[0]
            if len(out) > 0:
                if len(out[0]) > 0:
                    try:
                        self.color_chunk(out,chunk_start,chunk_size)
                        #print("Done with {0}".format((chunk_start,chunk_size)))
                    except:
                        print("Error! {0}".format((chunk_start,chunk_size)))
            
            if save_partial or c == chunks-1:
                im.putdata(sum(self.color_rows,[]))
                im.save(name)
            numdone += 1
            self.write_progress(numdone,chunks)
        
        print("..done.")


    def show_palette(self,dot_rad=1):
        r = dot_rad
        g = Graphics()
        for i in range(self.max_period+1):
            rgb = [float(c)/255.0 for c in self.palette[i]]
            #print(rgb)
            if i == 0:
                numstr = 'Unk'
                pos = self.max_period+1
            else:
                numstr = str(i)
                pos = i
            t = 1.2*2*r*pos
            d = disk((t,0),r,(0,2*pi),color=Color(rgb,space='rgb'),axes=False) + \
                text(numstr,(t,-1.75*r),fontsize=12,color='black')
            g += d
        return g


class PeriodPlot_old(SageObject):
    def __init__(self,iterations=50,plot_points=200,figsize=None,
                 seed=(1.0,0),eval_range=(-4,4),dome=True,seed_rad=.04,
                 giveup=15,estimate_threshold=.001,
                 chunk_size=100,partial_render=1,
                 h = cos):
        if figsize is None:
            figsize = int(plot_points/100)
        self.iterations = iterations
        self.plot_points = plot_points
        self.figsize = figsize
        self.dome = dome # only works for eval_range centered at 0
        self.dome_rad2 = float(eval_range[1])**2
        self.seed = seed
        self.seed_rad2 = seed_rad**2
        self.giveup = giveup
        self.estimate_threshold=estimate_threshold
        self.chunk_size = chunk_size
        self.partial_render = partial_render # number of partial renders to make, including final

        self.chunks_n = ceil(self.plot_points/self.chunk_size)
        
        #self.f = f2_slow
        self.f = f2
        self.h = h
        #self.f = cosines.test  #broken

        self.ranges = (eval_range,eval_range)
        xysetup = setup_for_eval_on_grid([],[eval_range],plot_points=plot_points)
        self.xvals = srange(*xysetup[1][0], include_endpoint=True)
        self.yvals = self.xvals #square image

        self.z_colors = np.zeros((self.plot_points,self.plot_points,3))
    def period_pt(self,k):
        try:
            m = 1 + int((k)/(self.giveup))
            m = float(m)
            al = 2.0*RDF(pi)*(k-1)/(self.giveup-1)
            z = (CC(cos(al),sin(al)))*(1+20**(k%2))
            return CC(z**m)
        except TypeError:
            return CC(0)
        
    def estimate_period(self, b):
        """
        Estimate the period for iterates of cos^b
        (giving up if period larger than self.give_up)
        If no period found, iterate more and try again
        Return color.
        """        
        if self.dome and b[0]**2 + b[1]**2 > self.dome_rad2:
            return self.color_point(None,None)
        if self.seed_rad2 > 0 and (b[0] - self.seed[0])**2 + (b[1] - self.seed[1])**2 < self.seed_rad2:
            return self.color_point(Infinity,Infinity)
        x0 = self.seed
        for n in range(1,4):
            x0,p = estimate_period_pyx(b,x0,self.iterations,h=self.h,
                                           max_period=self.giveup,epz=self.estimate_threshold)
            if p is not None:
                return self.color_point(p,n)
                
        return self.color_point(None,None)
        
    def subdivide(self,n,i,j):
        """
        chunk number i,j of n+1
        (i and j range from 0 to n; last piece may be empty)
        """
        p = int(self.plot_points/n)
        return (self.xvals[i*p:(i+1)*p],self.yvals[j*p:(j+1)*p])
    @parallel
    def eval_on_chunk(self,n,i,j):
        chunk = self.subdivide(n,i,j)
        verbose(chunk)
        z_values = [[ self.estimate_period(CC(a,-1*b)) for a in chunk[0]] for b in chunk[1]]
        verbose(z_values)
        return np.array(z_values)
        #return complex_to_rgb(np.array(z_values))
    def eval(self):
        pass
    def write_progress(self,k,n,pr,width=40):
        """
        write progress: k chunks of n**2 total
        pr = which partial render we're on
        """
        pdashes = int(1.*width*(n*pr+k)/n**2)+1
        bar = "-"*pdashes+" "*(width-pdashes)
        #p = float(100.*k/n)
        #print(pr,k,n)
        msg = "{0:03d}/{1:03d} [{2}]".format(n*pr+k+1,n**2,bar)
        sys.stdout.write(msg)
        sys.stdout.flush()
        sys.stdout.write("\b"*len(msg))
        sys.stdout.flush()
    def render_chunks(self,chunk_data,p,pr):
        rendered_chunks = {}
        num_done = 0
        for arg,out in self.eval_on_chunk(chunk_data):
            n,i,j = arg[0]
            self.write_progress(num_done,n,pr)
            #print((num_done,n**2))
            #print("..finished chunk ({0},{1})".format(j,i))
            #print(out)
            #print(z_colors[i*p:i*p+len(out), i*p:i*p+len(out),0:])
            #print([j*p,j*p+len(out),':',i*p,i*p+len(out[0])])
            if len(out) > 0:
                if len(out[0]) > 0:
                    try:
                        rendered_chunks[(n,i,j)] = out
                        num_done += 1
                    except:
                        print("Error again!")
                        print("skipping ".format((i,j)))
                        #raise
        sys.stdout.write('\n')
        print("now compiling!")
        for k,v in rendered_chunks.iteritems():
            n,i,j = k
            verbose("compiling "+str(k))
            self.z_colors[j*p:j*p+len(out),i*p:i*p+len(out[0]),:] = v

    def render_setup(self):
        n = max(int(self.plot_points/self.chunk_size),3)
        p = int(self.plot_points/n)
        print("Evaluating in {0} chunks (n = {1})".format(n**2,n))

        #print(z_colors.shape)
        ij_max = ceil(self.plot_points/p)
        chunk_data = [(n,i,j) for i in range(ij_max) for j in range(ij_max)]
        partial_size = ceil(n**2/self.partial_render)
        print("doing partial render every {0} chunks".format(partial_size))
        print(time.ctime())
        return n,p,chunk_data,partial_size

    def test_render(self,k=1):
        n,p,chunk_data,partial_size = self.render_setup()
        print("testing {0} chunk(s)".format(k))
        for _ in range(k):
            chunk = random.choice(chunk_data)
            print("testing chunk {0}".format(chunk))
            start = time.clock()
            a = self.eval_on_chunk(*chunk)
            D = time.clock() - start
            print("evaluation took {0}s".format(D))
            est = estimate_time(n**2,D)
            print("at this rate, total time will be {0}".format(est))

    def render(self,name=None):
        n,p,chunk_data,partial_size = self.render_setup()
        for pr in range(self.partial_render):
            partial_chunk_data = chunk_data[pr*partial_size:(pr+1)*partial_size]
            self.render_chunks(partial_chunk_data,p,pr)
            self.save(name=name)

    # def old_save(self,name=None):
    #     """
    #     slow.
    #     """
    #     if name is None:
    #         name = 'cosines_periods_{0}.png'.format(id_generator())

    #     g = Graphics()
    #     g._set_extra_kwds({'figsize':[self.figsize,self.figsize]})
    #     g.add_primitive(ComplexPlot(self.z_colors, *self.ranges, options={'interpolation':'catrom'}))
    #     print("saving to {0}..".format(name))
    #     g.save(name)
    #     print("..done.")

    def save(self,name=None):
        """
        no axes, margin, but faster
        """
        if name is None:
            name = 'img/cosines_periods_{0}.png'.format(id_generator())
        print("saving to {0}..".format(name))
        im = Image.new('RGB',(self.plot_points,self.plot_points))
        for i in range(self.plot_points):
            row = self.z_colors[i]
            im.putdata(row,offset=self.plot_points*i)
        im.save(name)
        print("..done.")



    def color_point(self,p,n):
        """
        number p with counter n
        return a color, as an rgb triple
        """
        if p is None:
            return [0., 0., 0.]
        elif p is Infinity:
            return [0., 0., 0.]
        h = float(1.0*p/(self.giveup-1))
        s = .7 + .25*(p%2)
        #v = 1 - min(1,e/self.estimate_threshold)
        v = .95*(1 - n/4)
        v = max(0,v)
        C = Color(h-.02,s,v, space='hsv')
        #print(C.rgb())
        return list(C.rgb())

    def palette(self,k):
        """
        return color number k
        """

    def show_palette(self):
        r = .1
        g = Graphics()
        e = 3.
        for n in range(1,self.giveup+1):
            if n == self.giveup:
                rgb = [0,0,0]
                numstr = 'Unk'
            else:
                rgb = self.color_point(n,0)
                numstr = str(n)
            t = 1.0*n/(self.giveup-1)
            d = disk((e*t,0),r,(0,2*pi),color=Color(rgb,space='rgb'),axes=False)+text(numstr,(e*t,-1.5*r),fontsize=12,color='black')
            g += d
        return g
