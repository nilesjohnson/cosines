"""
plot iterates of cos^b

Niles Johnson
"""

def f(b, x = RR(1), iterations = 100):
    """
    iterate cos^b
    """
    for i in range(iterations):
        x = cos(x)**b
    return x


def make_frames(start,end,seed=RR(1)):
    seed = RR(seed)
    print html("Frame N shows N iterates of cos^b, starting with x = {0}".format(seed))
    for n in range(start,end):
        print "working on frame {0}..".format(n)
        P = complex_plot(lambda b: f(b, x=seed, iterations=n), (-24,12), (0,24), plot_points=500, figsize=[4,4], aspect_ratio=1)
        P.save('cosine_frame_{0:06d}.png'.format(n))

        
        
#make_frames(200,205,seed=1)
