"""

OLD  NOW   BROKEN

DO NOT USE


plot iterates of cos^b

Niles Johnson


EXAMPLE:

sage: load('cosines2_view.py')
sage: P = PeriodPlot(iterations=100, plot_points=500, chunk_size=100)


sage: P.test_render(3)
Evaluating in 25 chunks (n = 5)
doing partial render every 25 chunks
testing 3 chunk(s)
testing chunk (5, 4, 1)
evaluation took 6.69s
at this rate, total time will be 0:00:06.690000
testing chunk (5, 4, 4)
evaluation took 6.61s
at this rate, total time will be 0:00:06.610000
testing chunk (5, 1, 4)
evaluation took 7.07s
at this rate, total time will be 0:00:07.070000


sage: P.render()
Evaluating in 25 chunks (n = 5)
doing partial render every 25 chunks
...
now compiling!
saving to img/cosines_periods_DE4ZX4.png with scipy.misc.toimage..
..done.






sage: trigs = [('cos',PeriodPlot(seed=(1,0),h=cos,**test_kwds)),('sin',PeriodPlot(seed=(1,0),h=sin,**test_kwds)),('exp',PeriodPlot(seed=(1,0),h=exp,**test_kwds))]

sage: for nam,S in trigs:                                                                     S.render(name='img/'+nam+'_periods.png')





Note: 8K Fulldome resolution is 8192 x 8192 [Wikipedia]

"""

import sys
from PIL import Image

test_kwds = {'iterations':700,
        'plot_points':2**8,
        'chunk_size':2**5,
        'eval_range':(-8,8),
        'dome':False,
        'seed_rad':0
        }
gen_kwds = {'iterations':1300,
        'plot_points':2**10,
        'chunk_size':2**7,
        'eval_range':(-8,8),
        'dome':False,
        'seed_rad':0
        }

from cosines2 import*
#load('cosines2.pyx')

#CC = sage.rings.complex_mpc.MPComplexField()
#CC = ComplexField(64)  #breaks horribly
CC = CDF   #fastest, but only 64 bit precision
def f2_slow(b, x = CC(1), iterations = 100, h = cos):
    """
    50 times slower, but arbitrary precision with MPComplexField
    
    sage: %timeit f2_slow(-1+I,1,4000)
    1 loops, best of 3: 296 ms per loop
    """
    b = CC(b)
    x = CC(x)
    for i in range(iterations):
        try:
            #x = CC(cos(x)**b)
            #x = CC(sin(x)**b)
            x = CC(h(x)**b)
        except:
            return CC(Infinity)
    # for j in range(show_next+1):
    #     # print values of x with only 32 bits of precision
    #     print "{0}th iterate (approx.): {1}".format(iterations+j,x.n(32))
    #     x = CC(cos(x)**b)
    return x

def f_Wynn(b, x = CC(1), iterations = 100, h = cos):
    """
    Using the Wynn epsilon-method accelerator
    """
    iterations += iterations % 2 # make even if not already
    b = CC(b)
    x = CC(x)
    
    epz = [[] for _ in range(iterations+1)]
    #epz[-1] = [0]*(iterations+2)
    epz[0] = []
    w = x
    for i in range(iterations+1):
        w = CC(h(w)**b)
        epz[0].append(w)
    epz[1] = [1/(epz[0][n+1] - epz[0][n]) for n in range(0,iterations)]
    for k in range(2,iterations+1):
        for n in range(0,iterations-k+1):
            if epz[k-1][n+1] == epz[k-1][n]:
                if k % 2:
                    # k is odd
                    print("Wynn converged")
                    return epz[k-1][n]
            epz[k].append(epz[k-2][n+1] + 1/(epz[k-1][n+1] - epz[k-1][n]))
    return epz[iterations][0]


def exp_interp(t):
    """
    interpolate from cos to I*sin, passing through 1/2 * exp
    """
    def h(s):
        return (1-t)*cos(s) + t*I*sin(s)
    return h


import numpy as np

import string
import random
def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))

from datetime import timedelta
def estimate_time(nprocs,proctime,ncpus=None):
    """
    Estimate time for parallel computation with ncpus, nprocs, and each process taking
    proctime.

    proctime is in seconds.

    Estimate is a rough guide -- more accurate when nprocs is very large compared to ncpus
    """
    if ncpus is None:
        ncpus = sage.parallel.ncpus.ncpus()
    s = proctime*nprocs/RDF(min(nprocs,ncpus))
    return str(timedelta(seconds=float(s)))


from sage.plot.misc import setup_for_eval_on_grid
from sage.plot.all import Graphics
from sage.plot.complex_plot import ComplexPlot, complex_to_rgb
#from scipy.misc import toimage
import time
class PeriodPlot(SageObject):
    def __init__(self,iterations=50,plot_points=200,figsize=None,
                 seed=(1.0,0),eval_range=(-4,4),dome=True,seed_rad=.04,
                 giveup=15,estimate_threshold=.001,
                 chunk_size=100,partial_render=1,
                 h = cos):
        if figsize is None:
            figsize = int(plot_points/100)
        self.iterations = iterations
        self.plot_points = plot_points
        self.figsize = figsize
        self.dome = dome # only works for eval_range centered at 0
        self.dome_rad2 = float(eval_range[1])**2
        self.seed = seed
        self.seed_rad2 = seed_rad**2
        self.giveup = giveup
        self.estimate_threshold=estimate_threshold
        self.chunk_size = chunk_size
        self.partial_render = partial_render # number of partial renders to make, including final

        self.chunks_n = ceil(self.plot_points/self.chunk_size)
        
        #self.f = f2_slow
        self.f = f2
        self.h = h
        #self.f = cosines.test  #broken

        self.ranges = (eval_range,eval_range)
        xysetup = setup_for_eval_on_grid([],[eval_range],plot_points=plot_points)
        self.xvals = srange(*xysetup[1][0], include_endpoint=True)
        self.yvals = self.xvals #square image

        self.z_colors = np.zeros((self.plot_points,self.plot_points,3))
    def period_pt(self,k):
        try:
            m = 1 + int((k)/(self.giveup))
            m = float(m)
            al = 2.0*RDF(pi)*(k-1)/(self.giveup-1)
            z = (CC(cos(al),sin(al)))*(1+20**(k%2))
            return CC(z**m)
        except TypeError:
            return CC(0)
        
    def estimate_period(self, b):
        """
        Estimate the period for iterates of cos^b
        (giving up if period larger than self.give_up)
        If no period found, iterate more and try again
        Return color.
        """        
        if self.dome and b[0]**2 + b[1]**2 > self.dome_rad2:
            return self.color_point(None,None)
        if self.seed_rad2 > 0 and (b[0] - self.seed[0])**2 + (b[1] - self.seed[1])**2 < self.seed_rad2:
            return self.color_point(Infinity,Infinity)
        x0 = self.seed
        for n in range(1,4):
            x0,p = estimate_period_pyx(b,x0,self.iterations,h=self.h,
                                           max_period=self.giveup,epz=self.estimate_threshold)
            if p is not None:
                return self.color_point(p,n)
                
        return self.color_point(None,None)
        
    def subdivide(self,n,i,j):
        """
        chunk number i,j of n+1
        (i and j range from 0 to n; last piece may be empty)
        """
        p = int(self.plot_points/n)
        return (self.xvals[i*p:(i+1)*p],self.yvals[j*p:(j+1)*p])
    @parallel
    def eval_on_chunk(self,n,i,j):
        chunk = self.subdivide(n,i,j)
        verbose(chunk)
        z_values = [[ self.estimate_period(CC(a,-1*b)) for a in chunk[0]] for b in chunk[1]]
        verbose(z_values)
        return np.array(z_values)
        #return complex_to_rgb(np.array(z_values))
    def eval(self):
        pass
    def write_progress(self,k,n,pr,width=40):
        """
        write progress: k chunks of n**2 total
        pr = which partial render we're on
        """
        pdashes = int(1.*width*(n*pr+k)/n**2)+1
        bar = "-"*pdashes+" "*(width-pdashes)
        #p = float(100.*k/n)
        #print pr,k,n
        msg = "{0:03d}/{1:03d} [{2}]".format(n*pr+k+1,n**2,bar)
        sys.stdout.write(msg)
        sys.stdout.flush()
        sys.stdout.write("\b"*len(msg))
        sys.stdout.flush()
    def render_chunks(self,chunk_data,p,pr):
        rendered_chunks = {}
        num_done = 0
        for arg,out in self.eval_on_chunk(chunk_data):
            n,i,j = arg[0]
            self.write_progress(num_done,n,pr)
            #print((num_done,n**2))
            #print("..finished chunk ({0},{1})".format(j,i))
            #print out
            #print z_colors[i*p:i*p+len(out), i*p:i*p+len(out),0:]
            #print [j*p,j*p+len(out),':',i*p,i*p+len(out[0])]
            if len(out) > 0:
                if len(out[0]) > 0:
                    try:
                        rendered_chunks[(n,i,j)] = out
                        num_done += 1
                    except:
                        print("Error again!")
                        print("skipping ".format((i,j)))
                        #raise
        sys.stdout.write('\n')
        print("now compiling!")
        for k,v in rendered_chunks.iteritems():
            n,i,j = k
            verbose("compiling "+str(k))
            self.z_colors[j*p:j*p+len(out),i*p:i*p+len(out[0]),:] = v

    def render_setup(self):
        n = max(int(self.plot_points/self.chunk_size),3)
        p = int(self.plot_points/n)
        print("Evaluating in {0} chunks (n = {1})".format(n**2,n))

        #print z_colors.shape
        ij_max = ceil(self.plot_points/p)
        chunk_data = [(n,i,j) for i in range(ij_max) for j in range(ij_max)]
        partial_size = ceil(n**2/self.partial_render)
        print("doing partial render every {0} chunks".format(partial_size))
        print(time.ctime())
        return n,p,chunk_data,partial_size

    def test_render(self,k=1):
        n,p,chunk_data,partial_size = self.render_setup()
        print("testing {0} chunk(s)".format(k))
        for _ in range(k):
            chunk = random.choice(chunk_data)
            print("testing chunk {0}".format(chunk))
            start = time.time()
            a = self.eval_on_chunk(*chunk)
            D = time.time() - start
            print("evaluation took {0}s".format(D))
            est = estimate_time(n**2,D)
            print("at this rate, total time will be {0}".format(est))

    def render(self,name=None):
        n,p,chunk_data,partial_size = self.render_setup()
        for pr in range(self.partial_render):
            partial_chunk_data = chunk_data[pr*partial_size:(pr+1)*partial_size]
            self.render_chunks(partial_chunk_data,p,pr)
            self.save(name=name)

    # def old_save(self,name=None):
    #     """
    #     slow.
    #     """
    #     if name is None:
    #         name = 'cosines_periods_{0}.png'.format(id_generator())

    #     g = Graphics()
    #     g._set_extra_kwds({'figsize':[self.figsize,self.figsize]})
    #     g.add_primitive(ComplexPlot(self.z_colors, *self.ranges, options={'interpolation':'catrom'}))
    #     print "saving to {0}..".format(name)
    #     g.save(name)
    #     print "..done."

    def save(self,name=None):
        """
        no axes, margin, but faster
        """
        if name is None:
            name = 'img/cosines_periods_{0}.png'.format(id_generator())
        print("saving to {0}..".format(name))
        im = Image.new('RGB',(self.plot_points,self.plot_points))
        for i in range(self.plot_points):
            row = self.z_colors[i]
            im.putdata(row,offset=self.plot_points*i)
        im.save(name)
        print("..done.")



    def color_point(self,p,n):
        """
        number p with counter n
        return a color, as an rgb triple
        """
        if p is None:
            return [0., 0., 0.]
        elif p is Infinity:
            return [0., 0., 0.]
        h = float(1.0*p/(self.giveup-1))
        s = .7 + .25*(p%2)
        #v = 1 - min(1,e/self.estimate_threshold)
        v = .95*(1 - n/4)
        v = max(0,v)
        C = Color(h-.02,s,v, space='hsv')
        #print C.rgb()
        return list(C.rgb())

    def show_palette(self):
        r = .1
        g = Graphics()
        e = 3.
        for n in range(1,self.giveup+1):
            if n == self.giveup:
                rgb = [0,0,0]
                numstr = 'Unk'
            else:
                rgb = self.color_point(n,0)
                numstr = str(n)
            t = 1.0*n/(self.giveup-1)
            d = disk((e*t,0),r,(0,2*pi),color=Color(rgb,space='rgb'),axes=False)+text(numstr,(e*t,-1.5*r),fontsize=12,color='black')
            g += d
        return g
