load('cosines2_view.py')

# 8K = 2**13 = 8192

def spiral(t,A,z=0):
    return A*t*(cos(t) + I*sin(t)) + z

duration = 12
frame_rate = 30
num_frames = duration*frame_rate

dir='img/render_test/'

kwds = {'iterations':500,#5000,
        'plot_points':2**8,
        'chunk_size':2**4,
        'eval_range':(-8,8),
        'dome':True,
        }
i = 0
for t in sxrange(0,1,1./num_frames):
    c = spiral(t,.5,0)
    msg = """
frame = {0:08d}
seed  = ({1: f}.. , {2: f}..)
""".format(i,c[0],c[1])
    print(msg)
    P = PeriodPlot(seed=c, **kwds)
    name = dir+"cos_seed_spiral_{0:08d}.png".format(i)
    P.render(name=name)
    i += 1
