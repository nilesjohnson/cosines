"""
plot iterates of cos^b

Niles Johnson
"""

import cosines
import numpy as np

import string
import random
def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))


def f(b, x = 1, iterations = 401):
    """
    iterate cos^b

    version defined in cosines.pyx is faster
    """
    b = CC(b)
    x = CC(x)
    for i in range(iterations):
        x = cos(x)**b
    return x

def estimate_period(b,x=CC(1),iterations=150,giveup=11):
    """
    Estimate the period for iterates of cos^b
    (giving up if period larger than 10)
    """
    f = cosines.test
    x = CDF(f(b,x,iterations))
    y = x
    for i in range(1,giveup):
        y = cos(y)**b
        if (y-x).abs() < .01:
            return i
    # give up
    return None

def show_period(giveup=15):
    def z(b):
        n = estimate_period(b,iterations=600,giveup=giveup)
        try:
            return (cos(2*pi*n/giveup) + I*sin(2*pi*n/giveup))*(.2 + 3*(n+1)%2)
        except TypeError:
            return Infinity
    Q = complex_plot(z,(-4,4),(-4,4),plot_points=1200,figsize=[15,15])
    Q.save('period_'+str(time.time())+'.png')

def h(b, start=200, step=1):
    x = f(b,CC(1),start)
    y = f(b,x,step)
    return list(y) + [abs(y-x)]

def h01(u,v):
    return h(CC((u,v)),step=1)[0]
def h11(u,v):
    return h(CC((u,v)),step=1)[1]
def h21(u,v):
    return h(CC((u,v)),step=1)[2]

def k(u,v, start=50, step=1):
    b = CC((u,v))
    x = f(b,CC(1),start)
    y = f(b,x,step)
    return abs(y-x)

from datetime import timedelta
def estimate_time(nprocs,proctime,ncpus=None):
    """
    Estimate time for parallel computation with ncpus, nprocs, and each process taking
    proctime.

    proctime is in seconds.
    """
    if ncpus is None:
        ncpus = sage.parallel.ncpus.ncpus()
    s = proctime*nprocs/RDF(ncpus)
    return str(timedelta(seconds=float(s)))

from sage.plot.misc import setup_for_eval_on_grid
from sage.plot.all import Graphics
from sage.plot.complex_plot import ComplexPlot, complex_to_rgb
from scipy.misc import toimage
class PeriodPlot(SageObject):
    def __init__(self,iterations=50,plot_points=200,figsize=None,
                 seed=1.0,eval_range=(-4,4),
                 giveup=15,estimate_threshold=.01,
                 chunk_size=50,partial_render=1):
        if figsize is None:
            figsize = int(plot_points/100)
        self.iterations = iterations
        self.plot_points = plot_points
        self.figsize = figsize
        self.seed = seed
        self.giveup = giveup
        self.estimate_threshold=.01
        self.chunk_size = chunk_size
        self.partial_render = partial_render # number of partial renders to make, including final

        self.chunks_n = ceil(self.plot_points/self.chunk_size)
        
        self.f = cosines.test

        self.ranges = (eval_range,eval_range)
        xysetup = setup_for_eval_on_grid([],[eval_range],plot_points=plot_points)
        self.xvals = srange(*xysetup[1][0], include_endpoint=True)
        self.yvals = self.xvals #square image

        self.z_colors = np.zeros((self.plot_points,self.plot_points,3))
    def period_pt(self,k):
        try:
            al = 2.0*RDF(pi)*(k-1)/(self.giveup-1)
            z = (CDF(cos(al),sin(al)))*(1+20**(k%2))
            return CDF(z)
        except TypeError:
            return 0
        
    def estimate_period(self, b):
        """
        Estimate the period for iterates of cos^b
        (giving up if period larger than 10)
        """        
        x = CDF(self.f(b,self.seed,self.iterations))
        y = x
        for i in range(1,self.giveup):
            y = cos(y)**b
            if (y-x).abs() < self.estimate_threshold:
                return self.period_pt(i)
        # give up
        return 0
        

    def subdivide(self,n,i,j):
        """
        chunk number i,j of n+1
        (i and j range from 0 to n; last piece may be empty)
        """
        p = int(self.plot_points/n)
        return (self.xvals[i*p:(i+1)*p],self.yvals[j*p:(j+1)*p])
    @parallel
    def eval_on_chunk(self,n,i,j):
        chunk = self.subdivide(n,i,j)
        z_values = [[ self.estimate_period(CDF(a,b)) for a in chunk[0]] for b in chunk[1]]
        return complex_to_rgb(np.array(z_values))
    def eval(self):
        pass
    def render_chunks(self,chunk_data,p):
        for arg,out in self.eval_on_chunk(chunk_data):
            n,i,j = arg[0]
            print "..finished chunk ({0},{1})".format(j,i)
            #print out
            #print z_colors[i*p:i*p+len(out), i*p:i*p+len(out),0:]
            #print [j*p,j*p+len(out),':',i*p,i*p+len(out[0])]
            if len(out) > 0:
                if len(out[0]) > 0:
                    try:
                        self.z_colors[j*p:j*p+len(out),i*p:i*p+len(out[0]),:] = out
                    except:
                        print "Error again!"
                        print out
                        raise
    def render(self):
        n = max(int(self.plot_points/self.chunk_size),3)
        p = int(self.plot_points/n)
        print "Evaluating in {0} chunks".format(n**2)

        #print z_colors.shape
        ij_max = ceil(self.plot_points/p)
        chunk_data = [(n,i,j) for i in range(ij_max) for j in range(ij_max)]
        partial_size = ceil(n**2/self.partial_render)
        print "doing partial render every {0} chunks".format(partial_size)

        for pr in range(self.partial_render):
            partial_chunk_data = chunk_data[pr*partial_size:(pr+1)*partial_size]
            self.render_chunks(partial_chunk_data,p)
            self.save()

    def old_save(self,name=None):
        """
        slow.
        """
        if name is None:
            name = 'cosines_periods_{0}.png'.format(id_generator())

        g = Graphics()
        g._set_extra_kwds({'figsize':[self.figsize,self.figsize]})
        g.add_primitive(ComplexPlot(self.z_colors, *self.ranges, options={'interpolation':'catrom'}))
        print "saving to {0}..".format(name)
        g.save(name)
        print "..done."

    def save(self,name=None):
        """
        no axes, margin, but faster
        """
        if name is None:
            name = 'cosines_periods_{0}.png'.format(id_generator())
        print "saving to {0} with scipy.misc.toimage..".format(name)
        toimage(self.z_colors).save(name)
        print "..done."

import time

def make_frames(start,end,seed=CC(1),xmin=-4,xmax=4,plot_points=400,figsize=8):
    for n in range(start,end):
        print "Working on frame {0}..".format(n)
        start = time.clock()
        f = cosines.test
        P = complex_plot(lambda b: f(b, seed, n),
                         (xmin,xmax), (xmin,xmax),
                         plot_points=plot_points,
                         figsize=[figsize,figsize])
        print "..plotting took %f seconds"%( time.clock() - start)
        start = time.clock()
        P.save('img/loper{0:08d}.png'.format(n))
        print "..saving took %f seconds"%( time.clock() - start)

