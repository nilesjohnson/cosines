"""
Iterate Re(sin(x)^b) 3 times and look at intersections with y = x.

Sage snippet below
"""

D = plot(x,(-4,4),color='black')
A = []
for b in sxrange(-2,2,.1):
    def f(x):
        x = CC(x)
        for _ in range(1,4):
            x = (CC(sin(x)**b)).real_part()
        return float(x)
    A.append(D+plot(f, (-4,4),ymax=4,ymin=-4,aspect_ratio = 1)+text('b = {0:.03f}'.format(float(b)),(2,-2),color='black'))
animate(A[::-1]+(5*[A[0]]))

