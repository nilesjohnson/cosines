"""
Compute iterates of cos^b. By Niles Johnson.

INSTRUCTIONS:  
1. Set input values b, x, N, k below.
2. Type Shift+Enter or press the Evaluate button.
3. Repeat!
"""

b = 1       # exponent for cosine
x = 1       # starting value of x
N = 400    # number of iterates to compute
k = 20      # show iterates N through N+k
def curve(t,A,B,a=0,b=0):
    return A*(sin(t))^2 + a + I*(B*(sin(t))^2 + b)
def spiral(t,A,z=0):
    return A*t*(cos(t) + I*sin(t)) + z

"""
The code below here defines a function to compute
iterates, and then does the computation using 
values above
"""

CC = CDF

def f(b, x = CC(1), iterations = 100, show_next=5):
    """
    iterate cos^b, showing next k values (default 5)
    """
    b = CC(b)
    x = CC(x)
    for i in range(iterations):
        x = CC(cos(x)**b)
    out = set([])
    for j in range(show_next+1):
        # print values of x with only 32 bits of precision
        #print "{0}th iterate (approx.): {1}".format(iterations+j,x.n(32))
        out.add(tuple(x.n(16)))
        x = CC(cos(x)**b)
    P = point2d(out,xmin=-10,xmax=10, ymin=-10, ymax=10, size=30, aspect_ratio=1)
    Q = P + point2d(b, color='red', size=40) + text("{0} points total".format(len(out)), (5,-2), color='black')
    return Q

        
# do the computation using values above
import time
start = time.clock()
#frames = [f(curve(t,4,0),x=x,iterations=N,show_next=k) for t in srange(0,pi,.2)]
#frames += [f(curve(t,-4,0),x=x,iterations=N,show_next=k) for t in srange(0,pi,.2)]
#frames += [f(curve(t,4,1),x=x,iterations=N,show_next=k) for t in srange(0,pi,.2)]


frames = [f(spiral(t,.5,1),x=x,iterations=N,show_next=k) for t in srange(0,6*pi,.2)]

A = animate(frames)
print "\n computation took {0} seconds".format(time.clock() - start)




