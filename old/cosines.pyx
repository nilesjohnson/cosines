# cosines.pyx
# cython: profile=True


import cython
#from sage.rings.complex_number import ComplexNumber
from sage.rings.complex_field import ComplexField
from sage.all import CDF
from sage.all import Color, pi
from sage.plot.complex_plot import complex_to_rgb
#import numpy as np
cimport numpy as np # for the special numpy stuff

CC = ComplexField(256)
@cython.profile(False)
cdef inline complex f(b,
                  double complex x0,
                  int iterations=50):
    cdef double z_real = 0., z_imag = 0.
    cdef int i
    x = CC(x0.real,x0.imag)
    b = CC(b)

    for i in range(0, iterations):
        x = x.cos()**(b)
    return complex(x.real(),x.imag())

def test(b,x0,N):
    return f(b,x0,N)

cdef phase(complex z0):
    z = CDF(z0)
    # test for NaN
    if str(z.real())[0] == 'N':
        return (0,1,1)
    else:
        c = Color((z.argument()/(2*pi),.5,z.abs()/1000))
        #c = Color((z.argument()/(2*pi),.5,.5))
    #print c
    return c.rgb()


@cython.boundscheck(False)
def create_fractal( double min_a,
                    double max_a,
                    double min_b,
                    int nb_iterations,
                    np.ndarray[double, ndim=3, mode="c"] image not None):

    cdef int width, height
    cdef int a, b, start_b, end_b
    cdef double real, imag, pixel_size

    width = image.shape[0]
    height = image.shape[1]

    pixel_size = (max_a - min_a) / width
    for a in range(width):
        real = min_a + a*pixel_size
        for b in range(height):
            imag = min_b + b*pixel_size
            colour = phase(f(complex(real,imag), 1.0, nb_iterations))
            image[a, b, 0] = float(colour[ 0 ])
            image[a, b, 1] = float(colour[ 1 ])
            image[a, b, 2] = float(colour[ 2 ])
