"""
Detect periodicity in a given sequence.

given sequence a, and number p,
let k be maximal so that k*p leq len(a)
take fft of a[-1*k*p:]
check whether frequencies at k, -k are larger than others
"""

def check_periodicity(a,p):
    k = int(len(a)/p)
    N = k*p
    b = FastFourierTransform(N)
    for i in range(N):
        b[i] = a[i - N]
    b.forward_transform()
    ## giving up -- this is going to be complicated
