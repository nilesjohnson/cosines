import time
import numpy as np

import cosines # this is the cython module that does the real work.


min_x = 2
max_x = 3
min_y = -.5
nb_iterations = 200
npix = 200

bytearray = np.zeros((npix, npix, 3))

import string
import random
def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))

def run(name=None):
    if name is None:
        name = 'cosines_'+id_generator()+'.png'
        print "saving to "+name
    start = time.clock()
    cosines.create_fractal(min_x, max_x, min_y, nb_iterations, bytearray)
    print "it took %f seconds to run"%( time.clock() - start)

    from scipy.misc import toimage
    toimage(bytearray).save(name)


run()
