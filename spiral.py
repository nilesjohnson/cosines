load('cosines3_view.py')

def kwds_spiral(t):
    k = good_P
    k['period_func_args']['a'] = CDF((1+2*t)*exp(2*pi*t*I))

    return k

@parallel
def spiral_frame(n,nmax):
    name = 'img/spiral/spiral_{0:04d}.png'.format(n)
    t = float(3.0*n/nmax)
    k = kwds_spiral(t)
    P = PeriodPlot(**k)
    P.save(name=name,chunks=10)
    return True

def render_spiral(nmax):
    g = spiral_frame([(n,nmax) for n in range(nmax+1)])
    for x in g:
        print 'done with {0}'.format(x[0][0])
