
MATHEMATICAL QUESTIONS
--

* Prove that the region with period 1 is really a cardiod
* Prove that the period doesn't depend on the initial seed value (or determine how it does)



ANIMATION IDEAS
--

* move seed point around unit circle
  UPDATE: Interestingly, moving the seed point only changes the region of 
  non-convergence.  I don't understand that too well, but I guess it makes sense
* zoom in on origin
* show plot filling in as we check for larger and larger periods
  (requires improvement)



FILES
--

* draw period plot for iterates of powers of cosine: cosines2_view.py
* animate period plots: simple_animation.sage


SETTINGS
--

* 8K fulldome resolution is 10**13 = 8192
* Good number of iterations seems to be 5000
* Good balance of parallel processing: 256 = 16**2 chunks
* Good largest range: (-8,8)


TIMING
--

Commit a794f17
Takes about 10 minutes for 5000 max iterations on each point and resolution 2**10
Takes about 1.5 hours for resolution 2**13



POST-PROCESSING
--

do some antialiasing with 'convert -blur', as in

convert -blur 0x.5 -virtual-pixel black cosines_fulldome_00000000.png cosines_fulldome_aa_00000000.png





ABOUT THE CODE
---------------------------

The idea is from a discussion on the department mailing list:  iterates of cos^b display different periodicity depending on b.  Let b be a complex number and define f(b) = period of iterates of cos^b, starting at x_0 = 1.

x_0 = 1
x_i = cos^b( x_{i-1} )

Assign a different color to each period, and plot a function of b.  In tests, the period seems not to depend on x_0, but I don't have a proof of that fact.

I did leave documentation in the code, but mostly just for myself, and not very thoroughly.  It's over a year old now, so I don't remember much.  I've gone over it to try to clarify a bit, and make sure at least something works.

To read the code, start in 'cosines2_view.py'.  This defines a sage object, PeriodPlot, which can make one of these images.  There's an example of using it at the top of the file.  The fractal function, f2_slow, is also defined there.  Other versions of f (f2, f3, and one called 'test') are defined in cosines.py, cosines.pyx, cosines2.pyx.  The pyx files are cython, and are automatically compiled to c by sage.  They used to work, but now they don't, and I don't understand the errors :(

PeriodPlot divides the output image into chunks so they can be plotted in parallel.  The pixel colors for each chunk are saved in a numpy array, and then converted to an image with scipy.toimage.  There is something about "partial render" too, which I haven't tested but I believe is there in case the target image is too large to be put together in one go.

Note:  I was momentarily confused by the coloring, because it uses sage's complex_plot.  This is a function which maps CC to a color wheel; the color function I described above is really a composite:

CC --> ZZ --> CC --> colors

where the first one is the period of iterates of cos^b, the last one is complex_plot, and the middle one is something I wrote (probably points on the unit circle or something).

There's a bunch of other stuff there too, including some mandelbrot stuff.  I think that was from a cython tutuorial I used.  I don't remember most of the rest of it -- sorry :/
